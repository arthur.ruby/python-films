import datetime
import json
import re


def count(filename: str) -> int:
    """Opens a JSON file and counts the number of elements in its structure."""
    records = load_records_from_json(filename=filename)
    return len(records)


def written_by(filename: str, writer: str) -> list:
    """Opens a JSON file and searches for a given writer's name."""
    records = load_records_from_json(filename=filename)
    return [record for record in records if record['Writer'] == writer]


def longest_title(filename: str) -> dict:
    """Opens a JSON file and searches for the longest titled movie."""
    records = load_records_from_json(filename=filename)
    longest_titled_film = {}
    longest_title = 0
    for record in records:
        if record["Type"] == "movie":
            if len(record["Title"]) > longest_title:
                longest_titled_film = record
                longest_title = len(record["Title"])
    return longest_titled_film


def best_rating(filename: str) -> dict:
    """Opens a JSON file and searches for the best Imdb rated record."""
    records = load_records_from_json(filename=filename)
    best_rated = {}
    best_imdb_rating = 0.0
    for record in records:
        if "imdbRating" in record and record["imdbRating"].replace(".", "").isdigit() and float(
                record["imdbRating"]) > best_imdb_rating:
            best_rated = record
            best_imdb_rating = float(record["imdbRating"])
    return best_rated


def latest_film(filename: str) -> str:
    """Opens a JSON file and searches for the latest film."""
    records = load_records_from_json(filename=filename)
    latest_film = {}
    date_regex = re.compile('[0-9]{2} [A-Z][a-z]{2} [0-9]{4}')
    latest_date: datetime = datetime.datetime(datetime.MINYEAR, 1, 1)
    for record in records:
        if record["Type"] == "movie" and date_regex.match(record["Released"]):
            formatted_record_datetime: datetime = datetime.datetime.strptime(record["Released"], "%d %b %Y")
            if formatted_record_datetime > latest_date:
                latest_film = record
                latest_date = formatted_record_datetime
    return latest_film["Title"]


def find_per_genre(filename: str, genre: str) -> list:
    """Opens a JSON file and searches for a given genre."""
    records = load_records_from_json(filename=filename)
    return [record for record in records if record["Genre"].__contains__(genre)]


def released_after(filename: str, searched_date: str) -> list:
    """Opens a JSON file and searches for all records released after a given date."""
    records = load_records_from_json(filename=filename)
    date_regex = re.compile('[0-9]{2} [A-Z][a-z]{2} [0-9]{4}')
    formatted_searched_datetime: datetime = datetime.datetime.strptime(searched_date, "%d/%m/%Y")
    return [record for record in records
            if date_regex.match(record["Released"])
            and datetime.datetime.strptime(record["Released"], "%d %b %Y") > formatted_searched_datetime]


def load_records_from_json(filename: str) -> dict:
    """Helper method to open a JSON file and load its data into a dictionary"""
    with open(file=filename, mode='r', encoding='utf-8') as json_file:
        return json.load(json_file)
